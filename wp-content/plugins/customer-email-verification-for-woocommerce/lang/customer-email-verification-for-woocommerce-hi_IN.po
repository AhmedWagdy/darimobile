msgid ""
msgstr ""
"Project-Id-Version: Customer Email verification for WooCommerce\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-04-22 11:41+0000\n"
"PO-Revision-Date: 2020-04-22 11:45+0000\n"
"Last-Translator: John\n"
"Language-Team: Hindi\n"
"Language: hi_IN\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.3.3; wp-5.4"

#: customer-email-verification-for-woocommerce.php:194
#: customer-email-verification-for-woocommerce.php:259
msgid "Sign Up Email Verification"
msgstr "ईमेल सत्यापन पर हस्ताक्षर करें"

#: customer-email-verification-for-woocommerce.php:260
#, php-format
msgid ""
"To verify your email a PIN was sent to <strong>%s</strong>. Please check "
"your inbox and enter the PIN below."
msgstr ""
"आपके ईमेल को सत्यापित करने के लिए एक पिन <strong>% s </ strong> भेजा गया था। "
"कृपया अपना इनबॉक्स देखें और नीचे पिन दर्ज करें।"

#: customer-email-verification-for-woocommerce.php:275
msgid "Invalid PIN Code"
msgstr "अमान्य पिन कोड"

#: customer-email-verification-for-woocommerce.php:282
#: includes/class-wc-customer-email-verification-admin.php:454
#: includes/class-wc-customer-email-verification-admin.php:592
msgid "Verify"
msgstr "सत्यापित करें"

#: customer-email-verification-for-woocommerce.php:289
msgid "Didn’t receive an email?"
msgstr "क्या आपको कोई ईमेल नहीं मिला है?"

#: customer-email-verification-for-woocommerce.php:289
#: includes/class-wc-customer-email-verification-admin.php:475
msgid "Resend Email"
msgstr "पुन: ईमेल भेजें"

#: includes/class-wc-customer-email-verification-admin.php:111
msgid "Customize"
msgstr "अनुकूलित करें"

#: includes/class-wc-customer-email-verification-admin.php:314
msgid "Enable customer email verification"
msgstr "ग्राहक ईमेल सत्यापन सक्षम करें"

#: includes/class-wc-customer-email-verification-admin.php:319
#: includes/customizer/class-cev-customizer.php:218
msgid "Email for Verification"
msgstr "सत्यापन के लिए ईमेल करें"

#: includes/class-wc-customer-email-verification-admin.php:322
msgid "Separate Verification Email"
msgstr "अलग से सत्यापन ईमेल"

#: includes/class-wc-customer-email-verification-admin.php:323
msgid "Verification in Account Email"
msgstr "खाता ईमेल में सत्यापन"

#: includes/class-wc-customer-email-verification-admin.php:330
msgid "Skip email verification for the selected user roles"
msgstr "चयनित उपयोगकर्ता भूमिकाओं के लिए ईमेल सत्यापन छोड़ें"

#: includes/class-wc-customer-email-verification-admin.php:337
msgid "Select email verification form theme color"
msgstr "ईमेल सत्यापन फॉर्म थीम रंग चुनें"

#: includes/class-wc-customer-email-verification-admin.php:353
msgid "Verification Message"
msgstr "सत्यापन संदेश"

#: includes/class-wc-customer-email-verification-admin.php:355
msgid "We sent you a verification email. Check and verify your account."
msgstr "हमने आपको एक सत्यापन ईमेल भेजा है। अपना खाता देखें और सत्यापित करें।"

#: includes/class-wc-customer-email-verification-admin.php:360
msgid "Verification Success Message"
msgstr "सत्यापन सफलता संदेश"

#: includes/class-wc-customer-email-verification-admin.php:362
msgid "Your Email is verified!"
msgstr "आपका ईमेल सत्यापित है!"

#: includes/class-wc-customer-email-verification-admin.php:367
msgid "Resend verification email message"
msgstr "सत्यापन ईमेल संदेश भेजें"

#: includes/class-wc-customer-email-verification-admin.php:369
msgid "You need to verify your account before login. {{cev_resend_email_link}}"
msgstr ""
"आपको लॉगिन करने से पहले अपने खाते को सत्यापित करना होगा। "
"{{Cev_resend_email_link}}"

#: includes/class-wc-customer-email-verification-admin.php:374
msgid "Message For Verified Users"
msgstr "सत्यापित उपयोगकर्ताओं के लिए संदेश"

#: includes/class-wc-customer-email-verification-admin.php:376
msgid "Your Email is already verified"
msgstr "आपका ईमेल पहले से ही सत्यापित है"

#: includes/class-wc-customer-email-verification-admin.php:381
msgid "Re-Verification Message"
msgstr "पुनः सत्यापन संदेश"

#: includes/class-wc-customer-email-verification-admin.php:383
msgid "A new verification link is sent. Check email. {{cev_resend_email_link}}"
msgstr "एक नया सत्यापन लिंक भेजा गया है। ईमेल देखें। {{Cev_resend_email_link}}"

#: includes/class-wc-customer-email-verification-admin.php:388
msgid "You can use following tag in email and message"
msgstr "आप ईमेल और संदेश में निम्नलिखित टैग का उपयोग कर सकते हैं"

#: includes/class-wc-customer-email-verification-admin.php:422
#: includes/class-wc-customer-email-verification-admin.php:575
msgid "Verification Status"
msgstr "सत्यापन की स्थिति"

#: includes/class-wc-customer-email-verification-admin.php:423
#: includes/class-wc-customer-email-verification-admin.php:588
msgid "Manual Verify"
msgstr "मैनुअल सत्यापन"

#: includes/class-wc-customer-email-verification-admin.php:424
#: includes/class-wc-customer-email-verification-admin.php:611
msgid "Verification Email"
msgstr "सत्यापन ईमेल"

#: includes/class-wc-customer-email-verification-admin.php:442
#: includes/class-wc-customer-email-verification-admin.php:579
msgid "Verified"
msgstr "सत्यापित"

#: includes/class-wc-customer-email-verification-admin.php:444
#: includes/class-wc-customer-email-verification-admin.php:581
msgid "Not Verified"
msgstr "प्रमाणित नहीं है"

#: includes/class-wc-customer-email-verification-admin.php:462
#: includes/class-wc-customer-email-verification-admin.php:600
msgid "Unverify"
msgstr "असत्यापित करें"

#: includes/class-wc-customer-email-verification-admin.php:532
msgid "Verification Email Successfully Sent."
msgstr "सत्यापन ईमेल सफलतापूर्वक भेजा गया।"

#: includes/class-wc-customer-email-verification-admin.php:541
msgid "User Verified Successfully."
msgstr "उपयोगकर्ता सत्यापित सफलतापूर्वक।"

#: includes/class-wc-customer-email-verification-admin.php:550
msgid "User Unverified."
msgstr "उपयोगकर्ता असत्यापित।"

#: includes/class-wc-customer-email-verification-admin.php:561
msgid "Resend Verification Email"
msgstr "सत्यापन ईमेल पुनः भेजे"

#: includes/class-wc-customer-email-verification-admin.php:571
#: includes/customizer/class-cev-customizer.php:206
msgid "Customer Email Verification for WooCommerce"
msgstr ""

#: includes/class-wc-customer-email-verification-admin.php:614
msgid "Send Verification Email"
msgstr "पुष्टिकरण ई - मेल भेजें"

#: includes/class-wc-customer-email-verification-email-common.php:123
msgid "User login"
msgstr "उपयोगकर्ता लॉगिन"

#: includes/class-wc-customer-email-verification-email-common.php:127
msgid "User display name"
msgstr "उपयोगकर्ता प्रदर्शन नाम"

#: includes/class-wc-customer-email-verification-email-common.php:131
msgid "User email"
msgstr "उपयोगकर्ता ईमेल"

#: includes/class-wc-customer-email-verification-email-common.php:135
#: includes/class-wc-customer-email-verification-email-common.php:185
msgid "Email Verification link"
msgstr "ईमेल सत्यापन लिंक"

#: includes/class-wc-customer-email-verification-email-common.php:139
msgid "Verification link"
msgstr "सत्यापन लिंक"

#: includes/class-wc-customer-email-verification-email-common.php:143
#: includes/class-wc-customer-email-verification-email-common.php:193
msgid "Resend Confirmation Email"
msgstr "पुष्टिकरण ईमेल पुन: भेजें"

#: includes/class-wc-customer-email-verification-email-common.php:147
msgid "Verification Pin"
msgstr "सत्यापन पिन"

#: includes/customizer/class-cev-customizer.php:247
#: includes/customizer/class-cev-customizer.php:270
msgid "Only for a separate verification email"
msgstr "केवल एक अलग सत्यापन ईमेल के लिए"

#: includes/customizer/class-cev-customizer.php:269
msgid "Email Heading"
msgstr "ईमेल शीर्षक"

#: includes/customizer/class-cev-customizer.php:292
msgid "Email Body"
msgstr "ईमेल बॉडी"

#: includes/customizer/class-cev-customizer.php:313
msgid "Available variables"
msgstr "Available variables"

#: includes/views/admin_options_customize.php:12
msgid "Customize the verification email"
msgstr "सत्यापन ईमेल को अनुकूलित करें"

#: includes/views/admin_options_customize.php:20
msgid "Launch Customizer"
msgstr "कस्टमाइज़र लॉन्च करें"

#: includes/views/admin_options_customize.php:31
msgid "Customize frontend messages"
msgstr "फ्रंटएंड संदेशों को अनुकूलित करें"

#: includes/views/admin_options_customize.php:42
#: includes/views/admin_options_settings.php:26
msgid "Save Changes"
msgstr "परिवर्तनों को सुरक्षित करें"

#: includes/views/admin_options_customize.php:44
#: includes/views/admin_options_settings.php:28
msgid "Data saved successfully."
msgstr "डेटा सफलतापूर्वक सहेजा गया।"

#: includes/views/admin_options_settings.php:15
msgid "General Settings"
msgstr "सामान्य सेटिंग्स"
