<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

require_once ABSPATH . '/wp-content/plugins/mstore-api/controllers/FlutterVendor.php';
require_once ABSPATH . '/wp-content/themes/storefront-child/custom-api.php';

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_locale_css' ) ):
    function chld_thm_cfg_locale_css( $uri ){
        if ( empty( $uri ) && is_rtl() && file_exists( get_template_directory() . '/rtl.css' ) )
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter( 'locale_stylesheet_uri', 'chld_thm_cfg_locale_css' );

// END ENQUEUE PARENT ACTION

// START WC Order statuses
function register_waiting_quote_order_status() {
    register_post_status( 'wc-waiting-quote', array(
        'label'                     => _x('Waiting quotation', 'Order status', 'woocommerce'),
        'public'                    => false,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Waiting quotation <span class="count">(%s)</span>', 'Waiting quotation span class="count">(%s)</span>', 'woocommerce' )
    ) );
}
add_action('init', 'register_waiting_quote_order_status');

function register_quote_created_order_status() {
    register_post_status( 'wc-quote-created', array(
        'label'                     => _x('Quotation Created', 'Order status', 'woocommerce'),
        'public'                    => false,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Quotation Created <span class="count">(%s)</span>', 'Quotation Created span class="count">(%s)</span>', 'woocommerce' )
    ) );
}
add_action('init', 'register_quote_created_order_status');

function register_quote_accepted_order_status() {
    register_post_status( 'wc-quote-accepted', array(
        'label'                     => _x('Quotation accepted', 'Order status', 'woocommerce'),
        'public'                    => false,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Quotation accepted <span class="count">(%s)</span>', 'Quotation accepted span class="count">(%s)</span>', 'woocommerce' )
    ) );
}
add_action('init', 'register_quote_accepted_order_status');

function register_quote_rejected_order_status() {
    register_post_status( 'wc-quote-rejected', array(
        'label'                     => _x('Quotation rejected', 'Order status', 'woocommerce'),
        'public'                    => false,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Quotation rejected <span class="count">(%s)</span>', 'Quotation rejected span class="count">(%s)</span>', 'woocommerce' )
    ) );
}
add_action('init', 'register_quote_rejected_order_status');

function add_order_statuses( $order_statuses ) {
 
    $new_order_statuses = array();
 
    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {
 
        $new_order_statuses[ $key ] = $status;
 
        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-waiting-quote'] = _x( 'Waiting quotation', 'Order status', 'woocommerce' );
            $new_order_statuses['wc-quote-created'] = _x( 'Quotation Created', 'Order status', 'woocommerce' );
            $new_order_statuses['wc-quote-accepted'] = _x( 'Quotation accepted', 'Order status', 'woocommerce' );
            $new_order_statuses['wc-quote-rejected'] = _x( 'Quotation rejected', 'Order status', 'woocommerce' );
        }
    }
 
    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'add_order_statuses' );

// END WC Order statuses

 if( class_exists('FlutterVendor') ) new Custom_Api();